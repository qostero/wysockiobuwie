var gulp = require('gulp'),
sass = require('gulp-sass'),
compressor = require('node-minify'),
autoprefixer = require('autoprefixer'),
sourcemaps = require('gulp-sourcemaps'),
postcss      = require('gulp-postcss');


var theme_dir = 'wp-content/themes/wysocki/';

gulp.task('sass', function () {
  return gulp.src(theme_dir + 'sass/style.sass') 
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest(theme_dir +'./css'));
});
 
gulp.task('sass:watch', function () {
	gulp.watch(theme_dir + 'sass/**/*.s+(a|c)ss', ['autoprefixer']);
});

gulp.task('default', function() {
  // place code for your default task here
});

gulp.task('compress-css', ['sass'], function() {
	compressor.minify({
	  compressor: 'clean-css',
	  input: theme_dir + 'css/style.css',
	  output: theme_dir + 'css/style-min.css',
	  options: {
	    advanced: false, // set to false to disable advanced optimizations - selector & property merging, reduction, etc. 
	    aggressiveMerging: false // set to false to disable aggressive merging of properties. 
	  },
	  callback: function (err, min) {}
	});	
});

gulp.task('autoprefixer', ['compress-css'], function () {
    return gulp.src(theme_dir + 'css/style-min.css')
    .pipe(sourcemaps.init())
    .pipe(postcss([ autoprefixer({ browsers: ['last 5 versions'] }) ]))
    .pipe(sourcemaps.write('.'))
	.pipe(gulp.dest(theme_dir + 'css/'));
});

gulp.task('compress-js', function () {
	compressor.minify({
	  compressor: 'no-compress',
	  input: [theme_dir + 'js/main-js.js' , theme_dir + 'js/ajax-loader.js'],
	  output: theme_dir + 'js/combined.js',
	  callback: function (err, min) {}
	});
	compressor.minify({ 
		compressor: 'uglifyjs',
		input: theme_dir + 'js/combined.js',
		output: theme_dir + 'js/main-js-min.js',
		callback: function (err, min) {}
	});
});