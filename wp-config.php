<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'wysockidb');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'root'); //wysocki: root

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', '');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q1Jm5[4V6q)fn0GK2~ei+zzWF|ze]|`B<a%OZH)b`u&g_GX19cK)5)TxALF[l*=6');
define('SECURE_AUTH_KEY',  '4-B%.?n6PIB>Ip:Bu&#}/ywTJJ~l0i0=c|KnUv(b%o^y>_OYen;x0$/8uds*Psj4');
define('LOGGED_IN_KEY',    '[_B#wLGc4g~lJG4~(haHyzJ`5Kv,>p;bZ_v|2s>5u}wJ!o?f6L!6BvrW2226n54M');
define('NONCE_KEY',        'b0#zwMAV}9H=]K?7%>:on,o@WXf`~r6I/@[R$4pk1%LLq7d-{vT,DC5@QIV>8U/X');
define('AUTH_SALT',        'D>{ede}KGEZb&SHQ4?hC#lh&O0{LU f$Tx|_]cd]C`kElY/}}1~(0@/B#Yop/>bK');
define('SECURE_AUTH_SALT', 'h}WQK_T6vqLtQ~w0xSBRH[Ok0[0anwhAo5C,2y37<<.7oB /W#Ic5Nv!CdbFY),0');
define('LOGGED_IN_SALT',   'OTl|)iLd(C3c#^$#(+:jy192E`m.S%pI2u@iWF!QP^;t&G5%WMCwt)di+k96X-bI');
define('NONCE_SALT',       'u8>7#5k[W_*su5f>JJ%|E]cjy):?btc72GQ$4*1*mnQ.|tiZn%L!aP_R/h|fzttv');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
