	<footer class="container-fluid bg-color">
		<section class="container">
			<div class="row info-row">
				<div class="col-xs-6 col-sm-4">
					<p>Mapa Strony:</p>
					<ul>
				       	<li><a href="#home">Home</a></li>
				        <li><a href="#kolekcja-butow">Kolekcje</a></li>
				        <li><a href="#o-firmie">O Firmie</a></li>
				        <li><a href="#materialy">Materiały</a></li>
				        <li><a href="#gdzie-kupic">Gdzie Kupić</a></li>
				        <li><a href="#kontakt">Kontakt</a></li>
					</ul>
				</div>
				<div class="col-xs-6 col-sm-4">
					<address>
						<p><span class="text-bold"> <?php the_field('city_name') ?></span></p>
						<p><span class="text-bold">ul.</span> <?php the_field('company_address') ?> </p>
						<p><span class="text-bold">tel.</span>  <?php the_field('phone_number') ?> </p>
						<p><span class="text-bold">e-mail:</span>  <?php the_field('email_company') ?> </p>
					</address>
				</div>
				<div class="col-xs-12 col-sm-4 social-box">
					<a  target="_blank" href="<?php the_field('link_fb') ?>" class="social-links">
						<img src="<?php echo get_template_directory_uri(); ?>/img/fb.png">
						<span>Facebook</span>						
					</a>
					<br>
					<a  target="_blank" href="<?php the_field('link_allegro') ?>" class="social-links">
						<img src="<?php echo get_template_directory_uri(); ?>/img/allegro.png">		
						<span>Allegro</p>						
					</a>
				</div>
			</div>
			<div class="row copyrights-row">
				<div class="col-md-12 copyrights-box">
					<p><a target="_blank" href="">Copyrights Bartłomiej Zalech</a></p> <p>|</p> <p><a target="_blank" href="http://tgdesigns.pl">Designed by Tomasz Górzkowski</a></p>
				</div>
			</div>
		</section>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>