jQuery(document).ready(function($){
	// grid = $('#post-listing');
	var exclude_posts = [];	
	var active_season;	

	function create_array_excluded_posts() {
		// if($('#season-ul li').hasClass('active')){
			selected_cat = $('#season-ul li.active').attr('data-filter');
			if (selected_cat == length) {
				selected_cat = selected_cat.replace(/\./g, '');
			}
		// }
		$('section .shoes-ajax .single-shoe').each(function() {
			var id = parseInt($(this).attr('data-id'));
			exclude_posts.push(id);
		});
	}

	$('#ajax-button').click(function(event) {	
		run_ajax();
	});

	function run_ajax() {
		create_array_excluded_posts();
		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {
				'action' : 'load_more_posts',
				category_name : selected_cat,
				exclude : exclude_posts
			},
			beforeSend: function() {
				$('#ajax-button span').html('Ładowanie produktów...');
			  },
			success: function(data) {
				if ($.trim(data)) {
					var $items = $(data);
					// $('.animated-loader').fadeOut(300).removeClass('active');
					setTimeout(function() {
						$('#ajax-button span').html('Pokaż więcej');
						$('#post-listing').append( $items ).isotope( 'appended', $items );
						// bLazy.revalidate(); //bLazy slow img load js framework
						// $('.blog-posts-filtered .post-inner a.title').dotdotdot({
						// 	height: 160,
						// });
						// $(window).data('post-loading', true);
					}, 1200);

				} else {
					$('#ajax-button span').html('wyświetlono wszystkie');
					// $('.loading-text').fadeOut(300, function() {
					// 	$('.loading-end').fadeIn(300);
					// $(window).data('post-loading', false);
					// });
				};
			},
			error: function(data) {
				console.log('blad w ajax');
				// $('.loading-text').fadeOut(300, function() {
				// 	$('.loading-end').fadeIn(300);
				// });
			}
			
		});
	}
	run_ajax();
	// --------------- //
	// 
	// init Isotope
	var $grid = $('#post-listing').isotope({
		itemSelector: '.single-shoe',
		layoutMode: 'none'
	});

	$('#season-ul').on( 'click', 'li', function() {
	  var filterValue = $( this ).attr('data-filter');
	  // use filterFn if matches value
	  $grid.isotope({ filter: filterValue });
		$('#ajax-button span').html('Pokaż Więcej');
		$("li").removeClass("active");
		$(this).toggleClass('active');
		$('.shoe-big-box').removeClass("active");

		if ($(this).hasClass('autumn')) {		
			$('.shoe-big-box.autumn').toggleClass('active');
			if ( $( ".shoe-small-box.autumn" ).length == 0) {
				run_ajax();
			}
		}
		if ($(this).hasClass('winter')) {
			$('.shoe-big-box.winter').toggleClass('active');
			if ( $( ".shoe-small-box.winter" ).length == 0) {
				run_ajax();
			}
		}
		if ($(this).hasClass('spring')) {
			$('.shoe-big-box.spring').toggleClass('active');
			if ( $( ".shoe-small-box.spring" ).length == 0) {
				run_ajax();
			}
		}
		if ($(this).hasClass('summer')) {
			$('.shoe-big-box.summer').toggleClass('active');
			if ( $( ".shoe-small-box.summer" ).length == 0) {
				run_ajax();
			}
		}
	});
	
});