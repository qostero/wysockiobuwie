jQuery(document).ready(function($){

Isotope.Item.prototype._create = function() {
  // assign id, used for original-order sorting
  this.id = this.layout.itemGUID++;
  // transition objects
  this._transn = {
    ingProperties: {},
    clean: {},
    onEnd: {}
  };
  this.sortData = {};
};

Isotope.Item.prototype.layoutPosition = function() {
  this.emitEvent( 'layout', [ this ] );
};

Isotope.prototype.arrange = function( opts ) {
  // set any options pass
  this.option( opts );
  this._getIsInstant();
  // just filter
  // this.filteredItems = this._filter( this.items );
  // flag for initalized
  this._isLayoutInited = true;
};

// layout mode that does not position items
Isotope.LayoutMode.create('none');
   $('.material-height, .contact-col').matchHeight({
	   	byRow: true,
	    property: 'height',
	    target: null,
	    remove: false
	  });
	$(window).scroll(function() {
		if (true) {}
	  if ($(document).scrollTop() > 50) {
	    $('nav , svg , .nav-style').addClass('shrink');

	  } else {
	    $('nav, svg, .nav-style').removeClass('shrink');

	  }
	});

$(document).on( "click", ".single-shoe",function(event) {
	event.preventDefault();
	var shoe_ID = $(this).attr('data-id');
	$.ajax({
		url: ajaxurl,
		type: 'POST',	
		data: {
			'action' : 'show_selected_shoe',
			shoe_selected: shoe_ID
		},
		beforeSend: function() {
			$('#shoe-more-info').empty();
			 var navOffset;
 			 navOffset = $('#navbar').height();
			$('html, body').animate({  scrollTop: $('#kolekcja-butow').offset().top - navOffset });
		},
		success: function(data) {
			if ($.trim(data)) {
				var $items = $(data);
				$('.single-hide').fadeOut("400");
				// setTimeout(function() {				
					$('#shoe-more-info').append( $items );
					$('#shoe-more-info').fadeIn('400');
				// }, 1200);

			} else {
				// $('#ajax-button span').html('wyświetlono wszystkie');
			};
		},
		error: function(data) {
			console.log('blad w ajax');
			// $('.loading-text').fadeOut(300, function() {
			// 	$('.loading-end').fadeIn(300);
			// });
		}
	});
	
		
});
$(document).on( "click", ".shoe-color-box",function() {
	var new_src = $(this).attr('data-imgsrc');
	$('#single-shoe-image').attr("src", new_src );
});
$(document).on( "click", "#close-btn",function() {
	$('#shoe-more-info').empty();
	$('.single-hide').fadeIn("400");
});


	// $('#season-ul').on( 'click', 'li', function() {
	// 	var filterValue = $(this).attr('data-filter');
 //  		$grid.isotope({ filter: filterValue });  		
	// });


    $("#owl-slider").owlCarousel({
		navigation : false, // Show next and prev buttons
		slideSpeed : 300,
		paginationSpeed : 400,
		items : 1, 
		itemsDesktop : false,
		itemsDesktopSmall : false,
		itemsTablet: false,
		itemsMobile : false,
		loop: $('#owl-slider').children().length > 1,
		autoplay:true,
		autoplayTimeout:5000,
		// autoplayHoverPause:true
  	});

  	//ESC key
  	$(document).keyup(function(e) {
	     if (e.keyCode == 27) { // escape key maps to keycode `27`
        	$('#shoe-more-info').empty();
			$('.single-hide').fadeIn("400");
	    }
	});

    // Smooth scroll
	$("nav ul li a[href^='#'], footer ul li a[href^='#'] ").on('click', function(event) {
	  var target;
	  target = this.hash;
		// target = target.replace(/\#/g, '');

	  event.preventDefault();
   	 var navOffset;
 	navOffset = $('#navbar').height();
	  

	  return $('html, body').animate({
	    scrollTop: $(this.hash).offset().top - navOffset
	  }, 300, function() {
	    return window.history.pushState(null, null, target);
	  });
	});

});
