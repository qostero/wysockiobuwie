<?php 

 function theme_add_style() {
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
	// wp_enqueue_style( 'style-css', get_template_directory_uri() . '/bootstrap/style.css' );
	wp_enqueue_style('owl.carousel-css',get_template_directory_uri().'/css/owl.carousel.css');
	wp_enqueue_style('main-css',get_template_directory_uri().'/css/style-min.css');

	
}

add_action( 'wp_enqueue_scripts', 'theme_add_style' );


function theme_add_scripts (){
	
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/plugins/jquery-3.0.0.min.js', array(), '1.0.0', false );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array(), '3.0.0', false );
	
	wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/js/plugins/owl.carousel.min.js', array(), '2.0.0', false );
	

	wp_enqueue_script('isotope', get_template_directory_uri() . '/js/plugins/isotope.pkgd.min.js', array(), '3.0.1', false );

	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main-js-min.js', array(), '1.0.0', false );
	wp_enqueue_script('match-height', get_template_directory_uri() . '/js/plugins/jquery.matchheight.js', array(), '1.0.0', false );
}

add_action( 'wp_enqueue_scripts', 'theme_add_scripts' );


/**
 * AJAX
 */
// $array_post_autumn_ids = array();	

// function ajax_js(){
// 	wp_enqueue_script( 'ajax-loader', get_template_directory_uri() . '/js/ajax-loader.js', array(), '1.0.0', true );


// }
// add_action( 'wp_enqueue_scripts', 'ajax_js' );

/**
 * Show single shoe, ajax
 * @return [html] [data single shoe]
 */
function show_selected_shoe(){
	$shoe_ID_chosen = $_POST['shoe_selected'];
	// $post = get_post($shoe_ID_chosen);
	 // setup_postdata( $post );
	 // To avoid ACF pro reapeater 
	$shoe_name = get_the_title($shoe_ID_chosen);
	$shoe_info = get_post_field('post_content', $shoe_ID_chosen);
	$shoe_number = get_field('shoe_number' , $shoe_ID_chosen);
	$shoe_link = get_field('shoe_link' , $shoe_ID_chosen);
	$shoe_size = get_field('shoe_size' , $shoe_ID_chosen);

	$shoe_color1 = get_field('kolor_nr_1' , $shoe_ID_chosen);
	$shoe_color2 = get_field('kolor_nr_2' , $shoe_ID_chosen);
	$shoe_color3 = get_field('kolor_nr_3' , $shoe_ID_chosen);
	$shoe_color4 = get_field('kolor_nr_4' , $shoe_ID_chosen);
	$shoe_color5 = get_field('kolor_nr_5' , $shoe_ID_chosen);
	$shoe_color6 = get_field('kolor_nr_6' , $shoe_ID_chosen);
	$shoe_color7 = get_field('kolor_nr_7' , $shoe_ID_chosen);
	$shoe_color8 = get_field('kolor_nr_8' , $shoe_ID_chosen);
	$shoe_color9 = get_field('kolor_nr_9' , $shoe_ID_chosen);
	$shoe_color10 = get_field('kolor_nr_10' , $shoe_ID_chosen);

	$shoe_img1 = get_field('zdjecie_nr_1' , $shoe_ID_chosen);
	$shoe_img2 = get_field('zdjecie_nr_2' , $shoe_ID_chosen);
	$shoe_img3 = get_field('zdjecie_nr_3' , $shoe_ID_chosen);
	$shoe_img4 = get_field('zdjecie_nr_4' , $shoe_ID_chosen);
	$shoe_img5 = get_field('zdjecie_nr_5' , $shoe_ID_chosen);
	$shoe_img6 = get_field('zdjecie_nr_6' , $shoe_ID_chosen);
	$shoe_img7 = get_field('zdjecie_nr_7' , $shoe_ID_chosen);
	$shoe_img8 = get_field('zdjecie_nr_8' , $shoe_ID_chosen);
	$shoe_img9 = get_field('zdjecie_nr_9' , $shoe_ID_chosen);
	$shoe_img10 = get_field('zdjecie_nr_10' , $shoe_ID_chosen);

		echo '<a id="close-btn" class="close-single"></a>';
	
		echo '<aside class="col-md-4 col-md-push-8"><h4>'. $shoe_name .'</h4><p class="shoe-number">'. $shoe_number .'</p><p class="shoe-info">'. $shoe_info .'</p>';
	if ($shoe_link != NULL) {
		echo '<p class="shoe-allegro">Ten model dostępny jest na allegro <a target="_blank" href="'. $shoe_link .'">tutaj</a></p> <a target="_blank" href="'. $shoe_link .'"><img src="' . get_template_directory_uri() . '/img/allegro.png"></a>';
	}
	if ($shoe_size != NULL) {
		echo '<p class="shoe-size-text">DOSTĘPNE ROZMIARY:</p><p class="shoe-sizes">'. $shoe_size .'</p>';
	}
		echo '<p class="shoe-colors">DOSTĘPNE KOLORY:</p>';
		echo '<div class="shoe-color-box" style="background-color:'. $shoe_color1 .' " data-imgsrc="'. $shoe_img1 .'"></div>';

		if($shoe_img2 != NULL && $shoe_color2 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color2 .' " data-imgsrc="'. $shoe_img2 .'"></div>';
		}		
		if($shoe_img3 != NULL && $shoe_color3 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color3 .' " data-imgsrc="'. $shoe_img3 .'"></div>';
		}	
		if($shoe_img4 != NULL && $shoe_color4 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color4 .' " data-imgsrc="'. $shoe_img4 .'"></div>';
		}
		if($shoe_img5 != NULL && $shoe_color5 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color5 .' " data-imgsrc="'. $shoe_img5 .'"></div>';
		}
		if($shoe_img6 != NULL && $shoe_color6 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color6 .' " data-imgsrc="'. $shoe_img6 .'"></div>';
		}
		if($shoe_img7 != NULL && $shoe_color7 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color7 .' " data-imgsrc="'. $shoe_img7 .'"></div>';
		}
		if($shoe_img8 != NULL && $shoe_color8 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color8 .' " data-imgsrc="'. $shoe_img8 .'"></div>';
		}
		if($shoe_img9 != NULL && $shoe_color9 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color9 .' " data-imgsrc="'. $shoe_img9 .'"></div>';
		}		
		if($shoe_img10 != NULL && $shoe_color10 != NULL){
			echo '<div class="shoe-color-box" style="background-color:'. $shoe_color10 .' " data-imgsrc="'. $shoe_img10 .'"></div>';
		}
		echo '</aside>';

		echo '<div class="col-md-8 col-md-pull-4 single-shoe-img-box"><img id="single-shoe-image" class="img-responsive" src='. $shoe_img1 .'></div>';
	wp_reset_postdata();

	die();	
}

add_action("wp_ajax_show_selected_shoe", "show_selected_shoe");
add_action("wp_ajax_nopriv_show_selected_shoe", "show_selected_shoe");


function load_more_posts(){

	$exclude_posts = $_POST['exclude'];
	$selected_cat = $_POST['category_name'];
	$counter = 0;
	$query = new WP_Query( array('post_type' => 'post' , 'posts_per_page'=> 4 , 'post_status' => 'publish' , 'category_name' => $selected_cat , 'post__not_in' => $exclude_posts ) ); // 
	// global $wpdb;
		if ( $query->have_posts() && $query != NULL) :
			echo '<div class="container"><div class="row">';	
			while ( $query->have_posts() ) : $query->the_post();
				$post_ID = get_the_ID();
				$post_img_url = get_field('zdjecie_nr_1');
				$categories = get_the_category( $post_ID );		
				$post_cat_slug = $categories[0]->slug;

				echo '<div class="col-sm-6 col-md-3 shoe-small-box small-box-'.++$counter.' single-shoe ' . $post_cat_slug . '" style="background-image: url(' . $post_img_url . ')"title="kliknij po więcej info" data-id="' . $post_ID . '"></div>';

			endwhile;
			echo '</div></div>';	
			/* Restore original Post Data */
		else :
			__return_zero();
		endif;
		wp_reset_postdata();

	die();	// this is required to terminate immediately and return a proper response
}

add_action("wp_ajax_load_more_posts", "load_more_posts");
add_action("wp_ajax_nopriv_load_more_posts", "load_more_posts");
/**
 * [wpb_add_google_fonts description]
 */
function wpb_add_google_fonts() {

wp_enqueue_style( 'wpb-google-fonts' , 'https://fonts.googleapis.com/css?family=Arima+Madurai|Josefin+Sans:300,400,700|Lato', false ); 
}

add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );


// Our custom post type function
function create_posttype() {

	register_post_type( 'Banner',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Banner' ),
				'singular_name' => __( 'Banner' )
			),
			'public' => true,
			'has_archive' => false,
			'rewrite' => array('slug' => 'Banner'),
		)
	);

		register_post_type( 'Material',
		array(
			'labels' => array(
				'name' => __( 'Material' ),
				'singular_name' => __( 'Material' )
			),
			'public' => true,
			'supports' => array( 'title', 'editor', 'thumbnail'),
			'use_featured_image' => true,
			'rewrite' => array('slug' => 'Material'),
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
add_theme_support( 'post-thumbnails' ); 




 ?>