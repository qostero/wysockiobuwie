<?php define( 'WP_USE_THEMES', false ); get_header(); ?>

<?php 

$posts_banner = get_field('slides_on_banner');  
$posts_material = get_field('materials_on_page'); 

$promo_autumn = get_field('promo_shoes_autumn');
$promo_winter = get_field('promo_shoes_winter');
$promo_spring = get_field('promo_shoes_spring');
$promo_summer = get_field('promo_shoes_summer');

$img_or_color_bg_page = get_field('radio_color_image_bg_page');
$bg_page_img = get_field('page_bg_img');
$bg_page_color = get_field('page_bg_color');

?>
<div class="bg-img-static" style="			
	<?php if ($img_or_color_bg_page == 'image') {
		echo 'background-image: url( ' . $bg_page_img . ' )';
	}
	 elseif ($img_or_color_bg_page == 'color') {
		echo 'background-color: ' . $bg_page_color . ' ';
	} ?>">
		

	</div>
<section class="container-fluid banner-container">
	<div  id="owl-slider"  class="main-slider owl-carousel">
		<?php if( $posts_banner ): ?>			
	    <?php foreach( $posts_banner as $post): // variable must be called $post (IMPORTANT) ?>
	        <?php setup_postdata($post); ?>
		
			<div class="banner-item" style="background-image: url( <?php the_field('slider_img'); ?>)">
				<h1><?php the_title(); ?></h1>				
			</div>

	    <?php endforeach; ?>	
		<?php endif; ?>	
		  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	</div>
</section>

<section id="kolekcja-butow" class="container shoes-container">
<?php 
	$seasons = get_field_object('seasons_active');	
	$value = $seasons['value'];
	$choices = $seasons['choices'];
	$active_season = get_field('season_active');

	
?>
	<div id="shoe-more-info" class="row shoe-selected bg-color-dark">

	</div>

	<?php if( $seasons != NULL ): 	 ?>
	<div class="row single-hide">
		<ul id="season-ul" class="season-tabs col-xs-12">
	<?php if( $value != NULL ): 	 ?>

			<?php foreach ($value as $v) : ?>
				<li data-filter=".<?php echo $v ; ?>" class="season-select <?php echo $v ; ?>
					<?php 			
					if ($active_season == $v ) {
						echo "active";
					} ?>"> <?php echo $choices[ $v ] ?> </li>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
	<?php endif; ?>

	<div id="post-listing" class="shoes-ajax row bg-color-dark single-hide">
<!-- Promo shoes -->
		<div class="container">
			<div class="promo-shoes row">
				<?php  
				if( $promo_autumn != NULL ): 	
				$counter = 0;		
				 foreach( $promo_autumn as $post): 
			        setup_postdata($post);	?>
					<div title="kliknij po więcej info" data-id="<?php echo $id ?>" class="col-sm-6 col-md-6  shoe-big-box single-shoe autumn  big-box-<?php echo ++$counter; ?> <?php if ($active_season == 'autumn') {
				echo "active";
				} ?> " style="background-image: url( <?php the_field('zdjecie_nr_1'); ?>)"> 
					
					<!-- <img src="<?php the_field('zdjecie_nr_1'); ?>" class="img-responsive"/> -->
					</div>	

				<?php  endforeach;	
				endif; 
				wp_reset_postdata(); ?>
				

				<?php  
				if( $promo_winter != NULL ): 
				$counter = 0;				
				 foreach( $promo_winter as $post): 
			        setup_postdata($post);	?>
					<div title="kliknij po więcej info" data-id="<?php echo $id ?>" class="col-sm-6 col-md-6  shoe-big-box single-shoe winter big-box-<?php echo ++$counter; ?> <?php if ($active_season == 'winter') {
				echo "active";
				} ?>" style="background-image: url( <?php the_field('zdjecie_nr_1'); ?>)">
					</div>			
				<?php  endforeach;	
				endif; 
				wp_reset_postdata(); ?>

				<?php  
				if( $promo_spring != NULL ): 
				$counter = 0;				
				 foreach( $promo_spring as $post): 
			        setup_postdata($post);	?>
					<div title="kliknij po więcej info" data-id="<?php echo $id ?>" class="col-sm-6 col-md-6  shoe-big-box single-shoe spring big-box-<?php echo ++$counter; ?> <?php if ($active_season == 'spring') {
				echo "active";
				} ?>" style="background-image: url( <?php the_field('zdjecie_nr_1'); ?>)">
					</div>			
				<?php  endforeach;	
				endif; 
				wp_reset_postdata(); ?>

				<?php  
				if( $promo_summer != NULL ): 
				$counter = 0;				
				 foreach( $promo_summer as $post): 
			        setup_postdata($post);	?>
					<div title="kliknij po więcej info" data-id="<?php echo $id ?>" class="col-sm-6 col-md-6  shoe-big-box single-shoe summer big-box-<?php echo ++$counter; ?> <?php if ($active_season == 'summer') {
				echo "active";
				} ?>" style="background-image: url( <?php the_field('zdjecie_nr_1'); ?>)">
					</div>			
				<?php  endforeach;	
				endif; 
				wp_reset_postdata(); ?>	
			</div>
		</div>
	</div>
	<div id="ajax-button" class="ajax-btn row single-hide">
		<span>Pokaż Więcej</span>
	</div>
</section>




<?php if (get_field('about_company') != NULL): ?>

<section id="o-firmie" class="container bg-color">
	<h2>O Firmie</h2>
	<div class="row">
		<div class="col-md-12">
			<article>
				<p>
					<?php the_field('about_company') ?>
				</p>
			</article>
		</div>
	</div>
</section>
<?php endif ?>

<?php if ($posts_material != NULL): ?>

<section id="materialy" class="container bg-color materials">
	<h2>Materiały</h2>
	<?php if( $posts_material ): ?>			
    <?php foreach( $posts_material as $post): // variable must be called $post (IMPORTANT) ?>
    <?php setup_postdata($post); ?>
	<div class="row">
		<div class="col-sm-8 col-md-8 material-height">
			<article class="material-text">
				<h3><?php the_title(); ?></h3>
				<p>
					<?php the_content(); ?>
				</p>
			</article>
		</div>
		<div class="col-sm-4 col-md-4 img-material-box material-height">
		<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
				<img class="material-img" src="<?php the_post_thumbnail_url() ?>">
		<?php } ?>
			
		</div>
	</div>
    <?php endforeach; ?>	
	<?php endif; ?>	
	<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>		
</section>
<?php endif ?>

<section id="gdzie-kupic" class="container bg-color sales">
	<h2>Gdzie Kupić?</h2>	
	<div class="row">
		<div class="col-md-12">
			<article class="first-text">
				<h3>Sprzedaż hurtowa</h3>
				<p>
					<?php echo the_field('whole_sales'); ?>
				</p>
			</article>
		</div>
		<div class="col-md-12">
			<article>
				<h3>Sprzedaż detaliczna</h3>
				<p>
					<?php echo the_field('retail_sales'); ?>
				</p>
			</article>
		</div>

	</div>
</section>

<section id="kontakt" class="container contact">
	<div class="row">
		<div class="col-md-6 contact-col bg-color">
			<h2>Adres</h2>	
			<article class="contact-text">				
				<p><span class="text-bold"> <?php the_field('city_name') ?></span></p>
				<p><span class="text-bold">ul.</span> <?php the_field('company_address') ?> </p>
				<p><span class="text-bold">tel.</span>  <?php the_field('phone_number') ?> </p>
				<p><span class="text-bold">e-mail:</span>  <?php the_field('email_company') ?> </p>
			</article>
			<h2>Formularz kontaktowy:</h2>	

			<?php echo do_shortcode( '[contact-form-7 id="7" title="Contact form 1"]' ); ?>
		</div>
		<div class="col-md-6 contact-col" id="map">			
			 <script>
			 	function windowWidth() {
					var wW = window.innerWidth;
					return (wW);
				}
		      function initMap() {
		        var uluru = {lat: 51.7522239, lng: 21.562143};
	        	var drgflag=false;
	        	var winWidth = windowWidth();
	        	if (winWidth > 768) {
	        		drgflag = true;
	        	}
		        var map = new google.maps.Map(document.getElementById('map'), {
		            zoom: 8,
					scrollwheel: false,
					draggable: drgflag,
					navigationControl: false,
					mapTypeControl: false,
					scaleControl: false,
		            center: uluru
		        });
		        var marker = new google.maps.Marker({
		          position: uluru,
                  icon: '<?php echo get_template_directory_uri(); ?>/img/marker.png ',
		          map: map
		        });
		      }
		    </script>
		    <script async defer
		    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr_oz422qprk78ybpZwMXhRRnMhfRW784&callback=initMap">
		    </script>
		</div>

	</div>
</section>

<?php get_footer(); ?>
